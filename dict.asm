%include "lib.inc"

section .text

global find_word
extern string_equals

%define DQ_SIZE 8

find_word:
	xor rax, rax
.loop:
	test rsi, rsi
	je .end
	push rsi
	push rdi
	add rsi, DQ_SIZE
	call string_equals
	pop rdi
	pop rsi
	
	test rax, rax
	jne .found
	mov rsi, [rsi]
	jmp .loop
.found:
	mov rax, rsi
	ret
.end:
	xor rax, rax
	ret
