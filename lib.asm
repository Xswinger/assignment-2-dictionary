section .text
global exit
global string_length
global print_string
global print_newline
global print_char
global print_uint
global print_int
global parse_uint
global string_equals
global string_copy
global parse_int
global read_char
global read_word

%define EXIT 60
%define SYS_CALL mov rax, 1
%define STDOUT 1
%define SPACE 32 
%define TAB 9
%define NEW_STRING 10

exit:
	mov rax, EXIT
	syscall

string_length:
	xor rax, rax
.count:
	cmp byte [rdi+rax], 0
	je .end
	inc rax
	jmp .count
.end:
	ret

print_string:
	call string_length
	mov rdx, rax
	SYS_CALL
	mov rsi, rdi
	mov rdi, STDOUT
	syscall
	ret

print_newline:
	mov rdi, 10
	call print_char

print_char:
	push rdi
	mov rax, 1
	mov rsi, rsp
	mov rdi, 1
	mov rdx, 1
	syscall
	pop rdi
	ret

print_uint:
	mov rax, rdi
	push 0
	mov r10, 10
.loop:
	mov rdx, 0
	div r10
	add rdx, 48
	push rdx
	cmp rax, r10
	jae .loop
	add rax, 48
	cmp rax, 48
	je .next
	push rax
.next:	
	pop rdi
	test rdi, rdi
	je .return
	call print_char
	jmp .next
.return:
	ret

print_int:
	mov r8, rdi
	test rdi, rdi
	jns .print
	mov rdi, '-'
	call print_char
	mov rdi, r8
	neg rdi
.print:
	call print_uint

parse_uint:
	xor rax, rax
	xor rcx, rcx
	xor rdx, rdx
.loop:
	mov dl, byte[rdi+rcx]
	cmp dl, 0
	js .out
	cmp dl, 10
	jns .out
	imul rax, 10
	add rax, rdx
	inc rcx
	jmp .loop
	ret
.out:
	mov rdx, rcx
	ret

string_equals:
	xor rcx, rcx
.loop:
	mov al, byte[rdi+rcx]
	mov dl, byte[rsi+rcx]
	cmp al, dl
	jne .not_equals
	inc rcx
	cmp al, 0
	je .equals
	jmp .loop
.equals:
	mov rax, 1
	ret
.not_equals:
	xor rax, rax
	ret

string_copy:
	push r8
	call string_length
	cmp rdx, rax
	jl .error
.loop:
	mov r8, 0
	mov r8b, byte[rdi]
	mov byte[rsi], r8b
	cmp r8, 0
	je .end
	inc rdi
	inc rsi
	jmp .loop
.error:
	xor rax, rax
.end:
	pop r8
	ret

parse_int:
	cmp byte[rdi], '-'
	jne .is_int
.uint:
	inc rdi
	call parse_uint
	neg rax
	inc rdx
	ret
.is_int:
	call parse_uint

read_char:
	xor rax, rax
	xor rdi, rdi
	mov rdx, 1
	push 0
	mov rsi, rsp
	syscall
	pop rax
	ret

read_word:
	push rdi
	push rsi
	mov rcx, rsi
.read_new_char:
	push rdi
	push rcx
	call read_char
	pop rcx
	pop rdi

	cmp al, TAB
	je .read_new_char
	cmp al, NEW_STRING
	je .read_new_char
	cmp al, SPACE
	je .read_new_char
.loop:
	cmp rcx, 1
	jle .error
	test rax, rax
	jle .end
	mov byte[rdi], al
	inc rdi
	dec rcx

	push rdi
	push rcx
	call read_char
	pop rcx
	pop rdi	
.check_space:
	cmp al, TAB
	je .end
	cmp al, NEW_STRING
	je .end
	cmp al, SPACE
	je .end
	jmp .loop
.end:
	pop rdx
	pop rax
	sub rdx, rcx
	mov byte[rdi], 0
	ret
.error:
	pop rdx
	pop rax
	xor rax, rax
	ret
