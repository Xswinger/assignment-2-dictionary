%include "lib.inc"
%include "colon.inc"
%include "words.inc"

%define BUFF_SIZE 255
%define DQ_SIZE 8

section .rodata
buffer_size: dq BUFF_SIZE

section .data
size_error: db "String syze is too large (max 255 symbols)"
no_string_error: db "String not found"

section .bss
buffer: resb BUFF_SIZE

section .text

global _start
extern find_word

_start:
	mov rdi, buffer
	mov rsi, buffer_size
	call read_word
	test rax, rax
	je .size_error

	mov rdi, buffer
	mov rsi, pointer
	push rsi
	call find_word
	pop rsi
	test rax, rax
	je .no_string_error

	add rax, DQ_SIZE
	mov rdi, rax
	call string_length
	add rdi, rax
	inc rdi
	call print_string
	call print_newline
	call exit
.size_error:
	mov rdi, size_error
	jmp .print_error
.no_string_error:
	mov rdi, no_string_error
.print_error:
	push rdi
	call string_length
	pop rsi

	mov rdx, rax
	mov rax, 1
	mov rdi, 2
	syscall
	call print_newline 
	call exit
