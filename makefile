NASM=nasm
FLAGS=-f elf64
LD=ld
.PHONY: clean
dict.o: dict.asm
	$(NASM) $(FLAGS) -o $@ $^

lib.o: lib.asm
	$(NASM) $(FLAGS) -o $@ $^

main.o: main.asm
	$(NASM) $(FLAGS) -o $@ $^

program: dict.o lib.o main.o
	$(LD) -o $@ $^

clean:
	rm *.o

cleanAll:
	rm *.o program

run:
	./program
